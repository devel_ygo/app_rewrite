package com.example.demo.comun.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DatosEntradaDto {
    private String url;
    private String rama;
    private String mensaje;
    private String usuario;
    private String clave;
}
