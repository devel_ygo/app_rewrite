package com.example.demo.servicios.archivos;

import lombok.extern.slf4j.Slf4j;
import com.example.demo.constants.Constants;
import com.example.demo.repository.SearchFilesRepository;
import com.example.demo.util.ValidationUtils;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

@Slf4j
@Service
public class EditBuildFilesServiceImpl implements EditBuildFilesService {

    private final SearchFilesRepository searchFilesRepository;

    @Autowired
    public EditBuildFilesServiceImpl(SearchFilesRepository searchFilesRepository) {
        this.searchFilesRepository = searchFilesRepository;
    }

    @Override
    public boolean addOpenRewriteConfigToProject(String filePath) {
        String buildFilePath;
        boolean isGradle = ValidationUtils.isGradleProject(filePath);
        boolean fileEdited;

        try {
            if (isGradle) {
                buildFilePath = filePath + File.separator + Constants.GRADLE_FILE;
                addOpenRewriteToGradleProject(buildFilePath);
            } else {
                buildFilePath = filePath + File.separator + Constants.MAVEN_FILE;
                addOpenRewriteToMavenProject(buildFilePath);
            }
            fileEdited = true;
        } catch (IOException e) {
            fileEdited = false;
        }

        return fileEdited;
    }

    @Override
    public void addOpenRewriteToGradleProject(String buildFilePath) throws IOException {
        String openRewritePlugin;
        String openRewriteBlock;
        String openRewriteDependency;
//        boolean isKotlinFile = searchFilesRepository.searchFileExtension(buildFilePath, ".kts");

        Path path = Path.of(buildFilePath);
        String content = Files.readString(path);
        StringBuilder modifiedContent = new StringBuilder(content);

        openRewritePlugin = "id(\"org.openrewrite.rewrite\") version(\"5.0.0\")";
        openRewriteBlock = "  activeRecipe(\"org.openrewrite.java.spring.boot2.UpgradeSpringBoot_2_7\")";
        openRewriteDependency = "\n    rewrite(platform(\"org.openrewrite.recipe:rewrite-recipe-bom:2.0.0\"))\n" +
                "    rewrite(\"org.openrewrite.recipe:rewrite-spring\")";

//        if (!isKotlinFile) {
//            openRewritePlugin = openRewritePlugin.replace("(\"", " '").replace("\")", "'");
//            openRewriteBlock = openRewriteBlock.replace("(\"", " '").replace("\")", "'");
//        }

        addOpenRewriteToBuildFile(content, Constants.PLUGINS_BLOCK_REGEX, Constants.PLUGINS_BLOCK_BEGIN, modifiedContent, openRewritePlugin);

        addOpenRewriteToBuildFile(content, Constants.REWRITE_BLOCK_REGEX, Constants.REWRITE_BLOCK_BEGIN, modifiedContent, openRewriteBlock);

        addOpenRewriteToBuildFile(content, Constants.DEPENDENCIES_BLOCK_REGEX, Constants.DEPENDENCIES_BLOCK_BEGIN, modifiedContent, openRewriteDependency);

        Files.writeString(path, modifiedContent.toString(), StandardOpenOption.TRUNCATE_EXISTING);
    }

    private void addOpenRewriteToBuildFile(String content, String regexBlock, String configBlockBegin, StringBuilder modifiedContent, String openRewriteBlock) {
        int closingBracketIndex;
        int configIndex = ValidationUtils.findMatchingOpeningBracket(content, regexBlock);
        if (configIndex != -1) {
            closingBracketIndex = ValidationUtils.findMatchingClosingBracket(content, configIndex);
            if (closingBracketIndex != -1) {
                modifiedContent.insert(closingBracketIndex, openRewriteBlock);
            }
        }
        else {
            modifiedContent.append("\n").append(configBlockBegin)
                    .append("\n\t").append(openRewriteBlock)
                    .append("\n").append(Constants.BRACKET_END)
                    .append('\n');
        }
    }

    @Override
    public void addOpenRewriteToMavenProject(String buildFilePath) throws IOException {
        Document document = DocumentHelper.createDocument();
        Element rootTag = document.addElement("project");
        Element buildTag = rootTag.addElement("build");
        Element pluginsTag = buildTag.addElement("plugins");

        Element openRewritePluginTag = pluginsTag.addElement("plugin");
        openRewritePluginTag.addElement("groupId").setText("org.openrewrite.maven");
        openRewritePluginTag.addElement("artifactId").setText("rewrite-maven-plugin");
        openRewritePluginTag.addElement("version").setText("5.0.0");

        Element openRewriteConfigurationTag = openRewritePluginTag.addElement("configuration");
        Element activeRecipesTag = openRewriteConfigurationTag.addElement("activeRecipes");
        activeRecipesTag.addElement("recipe").setText("org.openrewrite.java.spring.boot2.UpgradeSpringBoot_2_7");

        Element dependenciesTag = pluginsTag.addElement("dependencies");
        Element openRewriteDependencyTag = dependenciesTag.addElement("dependency");
        openRewriteDependencyTag.addElement("groupId").setText("org.openrewrite.recipe");
        openRewriteDependencyTag.addElement("artifactId").setText("rewrite-spring");
        openRewriteDependencyTag.addElement("version").setText("5.0.0");

        OutputFormat format = OutputFormat.createPrettyPrint();
        XMLWriter writer = new XMLWriter(new FileWriter(buildFilePath), format);
        writer.write(document);
        writer.close();
    }

}
