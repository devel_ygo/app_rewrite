package com.example.demo.servicios.archivos;

import java.io.IOException;

public interface EditBuildFilesService {

    boolean addOpenRewriteConfigToProject(String filePath) throws IOException;

    void addOpenRewriteToGradleProject(String buildFilePath) throws IOException;

    void addOpenRewriteToMavenProject(String buildFilePath) throws IOException;
}
