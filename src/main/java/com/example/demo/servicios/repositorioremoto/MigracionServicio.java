package com.example.demo.servicios.repositorioremoto;

import com.example.demo.comun.dto.DatosEntradaDto;
import com.example.demo.servicios.archivos.EditBuildFilesService;
import com.example.demo.servicios.directorios.ServicioDirectorio;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class MigracionServicio {

    @Autowired
    private ServicioRepositorio repositorio;
    @Autowired
    private ServicioDirectorio servicioDirectorio;
    @Autowired
    private EditBuildFilesService editFilesService;

    @Value("${directorio.local}")
    private String URL_LOCAL;
    @Value("${directorio.nuevaestructura.src}")
    private String URL_SRC;
    public void ejecutar(DatosEntradaDto datosEntrada) throws GitAPIException, IOException {

        servicioDirectorio.borrarDirectorio(URL_LOCAL);

        repositorio.clonar(datosEntrada.getUrl(), datosEntrada.getUsuario(), datosEntrada.getClave());
        repositorio.crearRama(datosEntrada.getRama());

        editFilesService.addOpenRewriteConfigToProject(URL_LOCAL);

        servicioDirectorio.crearDirectorio(URL_SRC);
        servicioDirectorio.MoverArchivos(URL_LOCAL+"src/", URL_SRC);

        repositorio.agregarCambios();
        repositorio.guardarCambios(datosEntrada.getMensaje());
        repositorio.subirCambios();
    }
}
