package com.example.demo.servicios.repositorioremoto;

import org.eclipse.jgit.api.errors.GitAPIException;

import java.io.IOException;

public interface Repositorio {
    void clonar (String url, String usuario, String clave) throws GitAPIException, IOException;
    void crearRama(String nombreRama) throws IOException, GitAPIException;
    void agregarCambios() throws GitAPIException;
    void guardarCambios(String mensaje) throws GitAPIException;
    void bajarCambios() throws GitAPIException;
    void subirCambios();
}
