package com.example.demo.servicios.repositorioremoto;

import lombok.extern.slf4j.Slf4j;
import org.eclipse.jgit.api.AddCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PushCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.PushResult;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Iterator;

@Slf4j
@Service
public class ServicioRepositorio implements Repositorio {
    private Repository repository;
    private Git git;
    private CredentialsProvider proveedorCredenciales;
    @Value("${directorio.local}")
    private String URL_LOCAL;

    public void clonar(String url, String usuario, String clave) throws GitAPIException {
        proveedorCredenciales = new UsernamePasswordCredentialsProvider(usuario, clave);
        git = Git.cloneRepository()
                .setURI(url)
                .setDirectory(new File(URL_LOCAL))
                .setCredentialsProvider(proveedorCredenciales).call();
    }

    @Override
    public void crearRama(String nombreRama) throws GitAPIException {
        git.checkout()
                .setCreateBranch(true)
                .setName(nombreRama).call();
    }

    @Override
    public void agregarCambios() throws GitAPIException {
        AddCommand add = git.add();
        add.addFilepattern(".").call();
    }

    @Override
    public void guardarCambios(String mensaje) throws GitAPIException {
        git.commit().setMessage(mensaje).call();
    }

    @Override
    public void bajarCambios() throws GitAPIException {
        git.pull().call();
    }

    @Override
    public void subirCambios() {
        PushCommand pc = git.push();
        pc.setCredentialsProvider(proveedorCredenciales).setForce(true).setPushAll();
        try {
            Iterator<PushResult> it = pc.call().iterator();
            if (it.hasNext()) {
                System.out.println(it.next().toString());
            }
        } catch (GitAPIException e) {
            e.printStackTrace();
        }
    }
}
