package com.example.demo.servicios.directorios;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Service
public class ServicioDirectorio {

    public void crearDirectorio (String directorio) throws IOException {
        Files.createDirectories(Paths.get(directorio));
    }
    public void borrarDirectorio (String directorio) throws IOException {
        File directory = new File(directorio);
        FileUtils.deleteDirectory(directory);
    }

    public void MoverArchivos (String directorioFuente, String directorioDestino) throws IOException {
        FileSystemUtils.copyRecursively(Paths.get(directorioFuente), Paths.get(directorioDestino));
        borrarDirectorio(directorioFuente);
    }
}
