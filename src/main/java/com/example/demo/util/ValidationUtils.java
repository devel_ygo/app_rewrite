package com.example.demo.util;

import lombok.experimental.UtilityClass;
import com.example.demo.constants.Constants;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@UtilityClass
public class ValidationUtils {

    public static boolean isMavenProject(String projectPath) {
        File pomXmlFile = new File(projectPath + File.separator + Constants.MAVEN_FILE);
        return pomXmlFile.exists();
    }

    public static boolean isGradleProject(String projectPath) {
        File buildGradleFile = new File(projectPath + File.separator + Constants.GRADLE_FILE);
        return buildGradleFile.exists();
    }

    public static int findMatchingClosingBracket(String content, int openingBracketIndex) {
        int closingBracketIndex = openingBracketIndex + 1;

        return content.substring(closingBracketIndex)
                .chars()
                .filter(c -> c == Constants.BRACKET_END.charAt(0))
                .findFirst()
                .orElse(-1) + openingBracketIndex + 2;
    }

    public static int findMatchingOpeningBracket(String content, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(content);

        if (matcher.find()) {
            return matcher.end();
        }

        return -1;
    }
}
