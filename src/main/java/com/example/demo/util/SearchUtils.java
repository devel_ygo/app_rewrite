package com.example.demo.util;

import lombok.experimental.UtilityClass;

import java.io.File;

@UtilityClass
public class SearchUtils {

    public static boolean findByFileName(File[] files, String fileName) {
        boolean found = false;

        for (File file : files) {
            found = file.isFile() && file.getName().equals(fileName);
            if(found){
                break;
            }
        }

        return found;
    }

    public static boolean hasExtension(File file, String extension) {

        return file.isFile() && file.getName().contains(extension);
    }
}
