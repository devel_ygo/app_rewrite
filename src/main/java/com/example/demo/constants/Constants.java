package com.example.demo.constants;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constants {

    public static final String MAVEN_FILE = "pom.xml";
    public static final String GRADLE_FILE = "build.gradle";
    public static final String DEPENDENCIES_BLOCK_BEGIN = "dependencies {";
    public static final String DEPENDENCIES_BLOCK_REGEX = "(?m)^dependencies \\{";
    public static final String PLUGINS_BLOCK_BEGIN = "plugins {";
    public static final String PLUGINS_BLOCK_REGEX = "(?m)^plugins \\{";
    public static final String REWRITE_BLOCK_BEGIN = "rewrite {";
    public static final String REWRITE_BLOCK_REGEX = "(?m)^rewrite \\{";
    public static final String BRACKET_BEGIN = "{";
    public static final String BRACKET_END = "}";
}
