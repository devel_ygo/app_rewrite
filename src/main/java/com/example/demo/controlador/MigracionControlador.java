package com.example.demo.controlador;

import com.example.demo.comun.dto.DatosEntradaDto;
import com.example.demo.servicios.repositorioremoto.MigracionServicio;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping(path = "/api/migrar")
//@Api(value = "Migrar app", description = "Recurso que realiza la modificación de la estructura del proyecto")
public class MigracionControlador {
    @Autowired
    private MigracionServicio servicio;

    @RequestMapping(value = "/init/",  method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    //@ApiOperation(value = "JSON con los parametros necesarios para la migración", notes = "Dado un JSON con la estructura predeterminada se procede con la modificación del ms" )
    public void migrarApp (@RequestBody DatosEntradaDto entrada) throws GitAPIException, IOException {
        servicio.ejecutar(entrada);
    }
}
