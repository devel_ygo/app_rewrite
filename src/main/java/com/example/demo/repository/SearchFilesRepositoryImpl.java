package com.example.demo.repository;

import com.example.demo.util.SearchUtils;
import org.springframework.stereotype.Repository;

import java.io.File;

@Repository
public class SearchFilesRepositoryImpl implements SearchFilesRepository {

    @Override
    public boolean searchFile(String filePath, String fileName) {
        File directory = new File(filePath);
        File[] files = directory.listFiles();

        return SearchUtils.findByFileName(files, fileName);
    }

    @Override
    public boolean searchFileExtension(String filePathAbsolute, String extension) {
        File file = new File(filePathAbsolute);

        return SearchUtils.hasExtension(file, extension);
    }



}
