package com.example.demo.repository;

public interface SearchFilesRepository {
    boolean searchFile(String filePath, String fileName);

    boolean searchFileExtension(String filePathAbsolute, String extension);
}
